import { Component,OnInit,ChangeDetectionStrategy,ViewChild,TemplateRef}from '@angular/core';
import { startOfDay,endOfDay,subDays,addDays,endOfMonth,isSameDay,isSameMonth,addHours} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent,CalendarDateFormatter,CalendarEventAction,CalendarEventTimesChangedEvent} from 'angular-calendar';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { CustomDateFormatter } from './custom-date-formatter.provider';
const colors: any = {
  green:{
    primary:'#32CD32',
    secondary:'#228B22',
  },
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})
export class DashboardComponent implements OnInit {
  selectUndefinedOptionValue:any;
  mins:any=1;
  hours:any=1;
  dataCondition:any=new Object();
  condition:any=new Object();
  dataStructure:any=new Object();
  currentUser:object;
  errString:string="";
  comments:string="";
  timeSheetData:any[]=new Array();
  myDate:Date=new Date();
  taskBackup:any[]=new Array();
  stageDisabled:boolean=true;
  taskDisabled:boolean=true;
  dataBackup:any;
  ngOnInit() {
  	console.log("OnInit");
  }
@ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: string = 'month';

  viewDate: Date = new Date();
  
  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [/*
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      actions: this.actions
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      actions: this.actions
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'A draggable and resizable event',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }*/
  ];


  activeDayIsOpen: boolean = false;

  constructor(public http:Http,private modal: NgbModal) {
    this.currentUser=JSON.parse(localStorage.getItem("currentUser"));
    this.timeSheetData=[{
      "project":"No entry yet",
      "stage":"No entry yet",
      "task":"No entry yet",
      "hours":"No entry yet",
      "mins":"No entry yet",
      "comments":"No entry yet"
    }];
    this.init();
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true)){
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
        this.myDate=date;
        this.init();  
      }
    }
  }
  
  today(date){
    this.viewDate = date;
    this.myDate=date;
    this.init();  
  }
  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }
project = ["Project 1", "Project 2", "Project 3","Project 4","Project 5","Project 6"];
projectSelected: any;

stages = ["Stage 1", "Stage 2", "Stage 3","Stage 4","Stage 5","Stage 6"];
stagesSelected: any;

task = ["Task 1", "Task 2", "Task 3","Task 4","Task 5","Task 6"];
taskSelected: any;

init(){
  var link="/getInitData?emailId="+this.currentUser["emailId"]+"&department="+this.currentUser["department"];
  this.http.get(link).map(res=>res.json()).subscribe(data=>{
    console.log(data);
    this.project=[];
    for(var i=0;i<data["data"]["Projects"].length;i++){
      if(data["data"]["Projects"][i]["Department"]==this.currentUser['department'])
      this.project.push(data["data"]["Projects"][i]["Name"]);
    }
    this.stages=data["data"]["Stages"];
    this.task=data["data"]["Tasks"];
    this.updateTable(data["timesheetData"]);
  },err=>{
    console.log(err);
  });
}
checkTaskShow(option){
  console.log("called");
  if(this.taskBackup.indexOf(option)!=-1)
  return true;
  else
  return false;
}
onOptionSelected(val,param){

 switch(param){
   case "projects":console.log(val,param);
   if(this.project.indexOf(this.projectSelected)!=-1)
   this.stageDisabled=false;
   this.taskClicked();
   break;
   case "stages":console.log(val,param);

   if(this.stages.indexOf(this.stagesSelected)!=-1)
   this.taskDisabled=false;
   this.taskClicked();
   break;
   case "tasks":console.log(val,param);
   
   break;
 }
}
taskClicked(){
  this.taskBackup=new Array();
  console.log(this.dataBackup);
  for(var i=0;i<this.dataBackup.length;i++){
    console.log(this.dataBackup[i]["EntryForDate"]);
   if(this.myDate.getFullYear()+"-"+((this.myDate.getMonth()+1)<10?'0'+(this.myDate.getMonth()+1):''+(this.myDate.getMonth()+1))+"-"+(this.myDate.getDate()<10?'0'+this.myDate.getDate():''+this.myDate.getDate())==this.dataBackup[i]["EntryForDate"].split("T")[0])
     if(this.dataBackup[i]["ProjectName"]==this.projectSelected && this.dataBackup[i]["StageName"]==this.stagesSelected && this.dataBackup[i]["Rejected"]!=1 )
       this.taskBackup.push(this.dataBackup[i]["Taskname"]);
   }
}
constructStructure(data){
  return {
    "Approved" : 0,
    "Rejected" : 0,
    "EntryForDate" : data.date,
    "ProjectName" : data.project,
    "StageName" : data.stage,
    "Taskname" : data.task,
    "TaskData" : {
      "hours" : data.hours,
      "minutes" : data.minutes,
      "comments" : data.comments
    }
  };
}

updateTable(data){
  console.log(data);  
  this.dataBackup=data;
  console.log(new Date("Mon Apr 16 2018 00:00:00 GMT+0530 (IST)").toDateString());
  var dateSelected=this.viewDate;
  this.timeSheetData=new Array(); 
  var datePresent=new Array();
  var dateObjectArray=new Array();
  
  for(var i=0;i<data.length;i++){
    if(datePresent.indexOf(data[i].EntryForDate.split("T")[0])==-1){
      datePresent.push(data[i].EntryForDate.split("T")[0]);
    dateObjectArray.push({"date":data[i].EntryForDate.split("T")[0],"tothrs":data[i].TaskData.hours,"totmins":data[i].TaskData.minutes,"approved":data[i].Approved,"rejected":data[i].Rejected});
    }else{
      dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["tothrs"]+=data[i].TaskData.hours;
      dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["totmins"]+=data[i].TaskData.minutes;
      if(dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["totmins"]>=60){
        dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["totmins"]-=60;
        dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["tothrs"]+=1;
      }
      if(data[i].Approved==1)
      dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["approved"]=1;
      if(data[i].Rejected==1)
      dateObjectArray[datePresent.indexOf(data[i].EntryForDate.split("T")[0])]["rejected"]=1;
    }
    if(dateSelected.setHours(0,0,0,0) == new Date(data[i].EntryForDate.split("T")[0]).setHours(0,0,0,0)){
      console.log(data[i]);
      var colorClass;
      if(data[i]["Approved"]==1)
      colorClass="success";
      if(data[i]["Approved"]==0 && data[i]["Rejected"]==1)
      colorClass="danger";
      else if(data[i]["Approved"]==0 && data[i]["Rejected"]==0)  
      colorClass="none";
      console.log(colorClass);
      this.timeSheetData.push({
        "project":data[i].ProjectName,
        "stage":data[i].StageName,
        "task":data[i].Taskname,
        "hours":data[i].TaskData.hours,
        "mins":data[i].TaskData.minutes,
        "comments":data[i].TaskData.comments,
        "classData":colorClass
      }); 
      
    }
    if(i==data.length-1){
        this.events=[];
        console.log(dateObjectArray);
        dateObjectArray.forEach(element => {
         console.log(element);
         var temp= {
          start: startOfDay(new Date(element.date)),
          title: 'Effort Required is '+element.tothrs+' hours '+element.totmins+' mins',
          color: colors.red,
        };
        if((element.tothrs+element.totmins)<=8.5)
        temp.color=colors.green;
        if((element.tothrs+element.totmins)>8.5)
        temp.color=colors.yellow;
        this.events.push(temp);
        if(element.approved==1)
        {
          temp= {
            start: startOfDay(new Date(element.date)),
            title: "You have some approved tasks",
            color: colors.blue, 
          };
          this.events.push(temp);
        }
        if(element.rejected==1)
        {
          temp= {
            start: startOfDay(new Date(element.date)),
            title: "You have some rejected tasks",
            color: colors.red,
          };
          this.events.push(temp);
        }        
        console.log(this.events);
       });
       }
}
}

submit(){
  this.errString="";
  if(this.project.indexOf(this.projectSelected)!=-1 && this.stages.indexOf(this.stagesSelected)!=-1 && this.task.indexOf(this.taskSelected)!=-1 && Number(this.hours)>=1 && Number(this.mins)>=1 )
  {
  this.dataCondition["$and"]=[{"emailId":this.currentUser["emailId"]},{"department":this.currentUser["department"]},{"data":{$elemMatch:{"EntryForDate":{"$regex":this.myDate.getFullYear()+"-"+((this.myDate.getMonth()+1)<10?'0'+(this.myDate.getMonth()+1):''+(this.myDate.getMonth()+1))+"-"+(this.myDate.getDate()<10?'0'+this.myDate.getDate():''+this.myDate.getDate())+"T00:00:00*"},"ProjectName":this.projectSelected,"Taskname":this.taskSelected,"StageName":this.stagesSelected}}}];
  this.condition["$and"]=[{"emailId":this.currentUser["emailId"]},{"department":this.currentUser["department"]}];
    console.log(this.viewDate);
  this.dataStructure=this.constructStructure({
    "date":this.myDate.getFullYear()+"-"+((this.myDate.getMonth()+1)<10?'0'+(this.myDate.getMonth()+1):''+(this.myDate.getMonth()+1))+"-"+(this.myDate.getDate()<10?'0'+this.myDate.getDate():''+this.myDate.getDate())+"T00:00:00",
    "project":this.projectSelected,
    "stage":this.stagesSelected,
    "task":this.taskSelected,
    "hours":Number(this.hours),
    "minutes":Number(this.mins),
    "comments":this.comments
  });
  console.log(this.dataStructure);
  var link="/updateTimeSheet";
  this.http.put(link,{"userCondition":JSON.stringify(this.condition),"condition":JSON.stringify(this.dataCondition),"data":JSON.stringify(this.dataStructure)}).map(res=>res.json()).subscribe(data=>{
    console.log(data);
    if(data.status=="SUCCESS")
    this.errString="Updated Successfully";
    var temp=this.viewDate;
    this.updateTable(data.data);
    this.viewDate=temp;
    this.projectSelected=this.selectUndefinedOptionValue;
    this.stagesSelected=this.selectUndefinedOptionValue;
    this.taskSelected=this.selectUndefinedOptionValue;
    this.stageDisabled=true;
    this.taskDisabled=true;
    this.hours=1;
    this.mins=1;
    this.comments="";
  },(error)=>{
    if(error=="DUPLICATE_ENTRY"){
      this.errString="Entry already exists";
    }
    if(error=="ERROR"){
      this.errString="Some error occurred";
    }
  });
}
else
{
  this.errString="Please check the options selected";
}
}
}
















