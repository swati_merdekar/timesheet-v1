import { Component, OnInit } from '@angular/core';
import { VERSION, MatMenuTrigger } from '@angular/material';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 selected = 'option2';
 userName:string="User";
  constructor() { }

  ngOnInit() {
    this.userName=JSON.parse(localStorage.getItem("currentUser")).name;
  }

}