import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AuthGuard } from './_guards/index';
import { AppRoutingModule } from './app-routing.module';
import { CalendarModule } from 'angular-calendar';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/index';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DemoUtilsModule } from './demo-utils/module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { customHttpProvider } from './_helpers/index';
import { HttpModule } from '@angular/http';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    CalendarModule.forRoot(),
    NgbModalModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    DemoUtilsModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    ReactiveFormsModule
  ],
  providers: [
  AuthGuard,
  customHttpProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
