import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailId:string="";
  password:string="";
  errData:string;
  errString:string="Invalid username or password.";
  currentUser:object=new Object();
  returnUrl:string;
  constructor(private route: ActivatedRoute,public router:Router,public http:Http) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  submit(){
    console.log(this.emailId +" "+this.password);
    if(!this.emailId.match(/[a-z0-9]+[@]{1}[a-z0-9]+[.]{1}[a-z0-9]+/i) || !this.password.match(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-z]).*$/)){
      console.log(this.errString);
      this.errData=this.errString;
      return;
    }
    else{
      this.errData="";
      var link="/login?emailId="+this.emailId+"&password="+this.password;
      this.http.get(link).map(res=>res.json()).subscribe(data=>{
        this.errData="";
        this.currentUser["token"]=data.token;
        
        this.currentUser["name"]=data.payload.name;
        this.currentUser["emailId"]=data.payload.emailId;
        this.currentUser["name"]=data.payload.name;
        this.currentUser["role"]=data.payload.role;
        this.currentUser["department"]=data.payload.department;    
        localStorage.setItem("currentUser",JSON.stringify(this.currentUser));   
         sessionStorage.setItem("timesheetData",JSON.stringify(data.payload.data));
         if(typeof(data.token)!=undefined && typeof(data.payload.emailId)!=undefined && data.payload.emailId==this.emailId )
         this.router.navigate([this.returnUrl]);
        },error=>{
        console.log(this.errString);
        this.errData=this.errString;
      });
    }
    console.log(this.emailId +" "+this.password);
  }
}
