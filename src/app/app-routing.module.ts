import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/index';
import { LoginComponent } from './login/index';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
{ path: '', component: DashboardComponent,canActivate: [AuthGuard]},
{ path: 'login', component: LoginComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
